package Bio::DB::Sam::MCE;

use strict;

#use 5.008_005;
our $VERSION = '0.01';

use Bio::DB::Sam;

use MCE::Flow;
use MCE::Queue;

use Data::Dumper;

my @SNPs;    # this will be list of SNPs
my %cov;     #this will be our hashref of coverage

my ( $max_consumers, $max_producer ) = ( 5, 1 );

my $que = MCE::Queue->new;
my $sam;     # each consumer will open handle

my $snp_caller = sub {
    my ( $seqid, $pos, $p ) = @_;

    #We are only covering one position
    
    my $segment = $sam->segment( $seqid, $pos, $pos );
    my ($coverage) = $segment->features('coverage');
    my @data_points = $coverage->coverage;

    $DB::single=2;

    foreach my $d (@data_points) {
        if ( $d <= 10 ) {
            MCE->gather('c10',  $cov{10} += 1);
        }
        elsif ( $d <= 20 ) {
            MCE->gather('c20',  $cov{20} += 1);
        }
        elsif ( $d <= 30 ) {
            MCE->gather('c30',  $cov{30} += 1);
        }
        elsif ( $d <= 40 ) {
            MCE->gather('c40',  $cov{40} += 1);
        }
        elsif ( $d <= 50 ) {
            MCE->gather('c50',  $cov{50} += 1);
        }
        elsif ( $d <= 60 ) {
            MCE->gather('c60',  $cov{60} += 1);
        }
        elsif ( $d <= 70 ) {
            MCE->gather('c70',  $cov{70} += 1);
        }
        elsif ( $d <= 80 ) {
            MCE->gather('c80',  $cov{80} += 1);
        }
        elsif ( $d <= 90 ) {
            MCE->gather('c90',  $cov{90} += 1);
        }
        elsif ( $d <= 100 ) {
            MCE->gather('c100',  $cov{100} += 1);
        }
        else {
            MCE->gather('c101',  $cov{101} += 1);
        }

        MCE->gather('ctotal',  $cov{ctotal} += 1);
    }

    #TODO benchmark against this command as well
    #$sam->coverage2BedGraph([$fh])

    $DB::single=2;
    #    my $refbase = $sam->segment($seqid,$pos,$pos)->dna;
    #    my ($total,$different) = (0,0);
    #    for my $pileup (@$p) {
    #        my $b = $pileup->alignment;
    #
    #        # don't deal with these ;-)
    #        next if $pileup->indel or $pileup->is_refskip;
    #
    #        my $qbase = substr($b->qseq,$pileup->qpos,1);
    #        next if $qbase =~ /[nN]/;
    #
    #        my $qscore = $b->qscore->[$pileup->qpos];
    #        next unless $qscore > 25;
    #
    #        $total++;
    #        $different++ if $refbase ne $qbase;
    #    }
    #    if ($total >= 4 && $different/$total >= 0.25) {
    #        MCE->gather(@SNPs,"$seqid:$pos");
    #    }
};

sub consumer {
    my ($mce) = @_;

    $sam = Bio::DB::Sam->new(
        -bam   => "/data/coredata/advcomp/1Kbam/ex1.bam",
        -fasta => "/data/coredata/advcomp/1Kbam/ex1.fa",
    );

    while ( my $id = $que->dequeue ) {
        $sam->pileup( $id, $snp_caller );
    }

    return;
}

mce_flow {
    gather      => \%cov,
    max_workers => [ 5, 1 ],

  },
  \&consumer, sub {
    my ($mce) = @_;    # producer

    $que->enqueue('seq1');
    $que->enqueue('seq2');

    $que->enqueue( (undef) x $max_consumers );
  };

#print "Found SNPs: @SNPs\n";

print "Coverage stats ".Dumper(\%cov)."\n";

1;

__END__

=encoding utf-8

=head1 NAME

Bio::DB::Sam::MCE - Blah blah blah

=head1 SYNOPSIS

  use Bio::DB::Sam::MCE;

=head1 DESCRIPTION

Bio::DB::Sam::MCE is

=head1 AUTHOR

Jillian Rowe E<lt>jillian.e.rowe@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2015- Jillian Rowe

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

=cut
